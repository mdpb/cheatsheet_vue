import { shallowMount, createLocalVue } from "@vue/test-utils";
import App from "@/App.vue";
import Navbar from "@/components/Navbar";
import Vue from "vue";
import vuetify from "vuetify";
import VueRouter from "vue-router";
import GlobalEvents from "vue-global-events";

Vue.use(vuetify);

describe("Main App", () => {
  let wrapper;
  beforeEach(() => {
    const localVue = createLocalVue();
    localVue.use(vuetify);
    localVue.use(VueRouter);
    localVue.component("GlobalEvents", GlobalEvents);

    wrapper = shallowMount(App, {
      localVue,
    });
  });

  test("loads properly", () => {
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});

describe("Navbar", () => {
  let wrapper;
  beforeEach(() => {
    const localVue = createLocalVue();
    localVue.use(vuetify);
    localVue.use(VueRouter);
    localVue.component("GlobalEvents", GlobalEvents);

    wrapper = shallowMount(Navbar, {
      localVue,
    });
  });

  test("buttons are available", () => {
    let home_btn = wrapper.get("#home");
    let about_btn = wrapper.get("#about-page");
    expect(home_btn.exists).toBeTruthy();
    expect(about_btn.exists).toBeTruthy();
  });
});
