import { mutations } from "@/store/mutations";
import { getters } from "@/store/getters";
import { actions } from "@/store/actions";
import { state } from "@/store/state";

export function createStore() {
  return {
    state,
    actions,
    mutations,
    getters,
  };
}

export function createPopulatedStore() {
  const state = {
    cur_cheatsheet: null,
    cheatsheets: [
      {
        uuid: 1234,
        title: "sasquatch lives among us",
        body: "I believe",
      },
      {
        uuid: 3131,
        title: "My big fat greek uncle",
        body: "A history of bad spoof comedies",
      },
    ],
  };

  return {
    actions,
    mutations,
    getters,
    state,
  };
}
