import { createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import { createStore, createPopulatedStore } from "./create-store";

const localVue = createLocalVue();
localVue.use(Vuex);

describe("Store", () => {
  test("nulls out cur_cheatsheet when active cheatsheet is deselected", () => {
    const storeConfig = createStore();
    const store = new Vuex.Store(storeConfig);

    store.dispatch("select_cheatsheet", { title: "active cheatsheet" });
    store.dispatch("select_cheatsheet", { title: "active cheatsheet" });
    expect(store.getters.cur_cheatsheet).toBe(null);
  });

  test("selects proper cheatsheet", () => {
    const storeConfig = createStore();
    const store = new Vuex.Store(storeConfig);

    let target_cheatsheet = {
      title: "sasquatch lives among us",
      body: "I believe",
    };

    store.dispatch("select_cheatsheet", { cheatsheet: target_cheatsheet });
    let selected = store.getters.cur_cheatsheet;
    expect(selected).not.toBeUndefined();
    expect(selected["cheatsheet"].title).toBe("sasquatch lives among us");
  });

  test("updates cheatsheet state when new cheatsheet is created", () => {
    const storeConfig = createPopulatedStore();
    const store = new Vuex.Store(storeConfig);

    let new_cheatsheet = {
      title: "otters!",
      body: "they are cute and fuzzy! They love the water and are playful.",
    };

    store.dispatch("save_cheatsheet", { cheatsheet: new_cheatsheet });
    let cheatsheets = store.getters.get_latest_cheatsheets;
    expect(cheatsheets[2]["cheatsheet"].title).toContain("otters");
  });
});
