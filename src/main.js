import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import GlobalEvents from "vue-global-events";
import VueCompositionApi from "@vue/composition-api";
import ElectronStore from "electron-store";

const fileStore = new ElectronStore({
  name: "local-cheatsheets",
  clearInvalidConfig: false,
});

Vue.use(VueCompositionApi);
Vue.component("GlobalEvents", GlobalEvents);
Vue.config.productionTip = false;
Vue.prototype.$fileStore = fileStore;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
