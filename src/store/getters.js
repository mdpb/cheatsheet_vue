export let getters = {
  edit_mode: (state) => {
    return state.edit_cheatsheet;
  },

  cur_cheatsheet: (state) => {
    return state.cur_cheatsheet;
  },

  get_latest_cheatsheets: (state) => {
    return state.cheatsheets;
  },

  new_draft: (state) => {
    return state.new_draft;
  },
};
