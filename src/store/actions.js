export let actions = {
  toggle_edit_mode({ commit }, route) {
    if (route === "Home") {
      commit("TOGGLE_EDIT_MODE");
    }
  },

  select_cheatsheet({ commit, state }, cheatsheet) {
    // deselect the cheatsheet and null out the state
    if (state.cur_cheatsheet === null) {
      commit("CUR_CHEATSHEET", cheatsheet);
    } else if (state.cur_cheatsheet.title === cheatsheet.title) {
      commit("CUR_CHEATSHEET", null);
    } else if (state.cur_cheatsheet.title !== cheatsheet.title) {
      commit("CUR_CHEATSHEET", cheatsheet);
    }
  },

  save_cheatsheet({ commit, state }, cheatsheet) {
    let cheatsheets = state.cheatsheets;
    cheatsheets.push(cheatsheet);
    commit("SAVE_CHEATSHEETS", cheatsheets);
  },

  recache_cheatsheets({ commit }, cheatsheets) {
    commit("SAVE_CHEATSHEETS", cheatsheets);
  },

  update_draft({ commit }, cheatsheet) {
    commit("NEW_DRAFT", cheatsheet);
  },
};
