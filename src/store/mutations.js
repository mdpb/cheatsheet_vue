export const mutations = {
  TOGGLE_EDIT_MODE(state) {
    state.edit_cheatsheet = !state.edit_cheatsheet;
  },

  CUR_CHEATSHEET(state, cheatsheet) {
    state.cur_cheatsheet = cheatsheet;
  },

  SAVE_CHEATSHEETS(state, cheatsheets) {
    state.cheatsheets = cheatsheets;
  },

  NEW_DRAFT(state, cheatsheet) {
    state.new_draft = cheatsheet;
  },
};
